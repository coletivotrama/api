# Backend do trama

## Pré-Requisitos

- Ter o [pip](https://pip.pypa.io/en/stable/installing/) instalado
- Ter o [virtualenv](https://virtualenv.readthedocs.org/en/latest/installation.html) instalado


## Para executar a aplicação pela primeira vez

- Executar os seguintes comandos
  - *virtualenv env*
  > Cria o ambiente virtual

  - *source env/bin/activate*
  > Ativa o ambiente virtual

  - *pip install -r requiriments.pip*
  > Instala as dependências


## Para executar o programa
  - Executar os seguinte comandos
    - *source env/bin/activate*
    - *python main.py
