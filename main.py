from flask import Flask, jsonify, url_for, redirect, request
from flask_pymongo import PyMongo
from flask_restful import Api, Resource
import json
from bson import json_util
from bson.objectid import ObjectId


app = Flask(__name__)
app.config["MONGO_DBNAME"] = "trama_db"
mongo = PyMongo(app, config_prefix='MONGO')
APP_URL = "http://127.0.0.1:5000"


class Event(Resource):
    def get(self,event_id):
        print event_id
        found=mongo.db.event.find_one({"_id": ObjectId(event_id)})
        print found
        return json_util.dumps(found)
    def put(self,event_id):
        data = request.get_json()
        if not data:
            data={"response":"ERROR"}
            return data,400
        elif mongo.db.event.find_one({"_id":ObjectId(event_id)}):
                mongo.db.event.update_one({"_id":ObjectId(event_id)},{"$set":data})
        return 200
    def delete(self,event_id):
        if not(event_id):
            return{"response":"ERROR"},400
        else:
            deleted=mongo.db.event.remove({"_id":ObjectId(event_id)})
            return deleted,200

class EventList(Resource):
    def get(self):
        data=[]
        cursor = mongo.db.event.find({})
        for event in cursor:
            print event
            ev=json_util.dumps(event)
            data.append(ev)
        return data,200, \
        {'Access-Control-Allow-Origin':'*',\
        }
    def post(self):
        data=request.get_json()
        if not data:
            data={"response":"ERROR"}
            return data,400
        else:
            nid=mongo.db.event.insert(data)
            print "novoid %s" % nid
            return {"id":str(nid)}
    def options (self):
        return {'Allow' : 'PUT' }, 200, \
        { 'Access-Control-Allow-Origin': '*', \
        'Access-Control-Allow-Methods' : 'PUT,GET' }

class Index(Resource):
    def get(self):
        return "works"

api = Api(app)
api.add_resource(Index,'/')
api.add_resource(EventList, '/events')
api.add_resource(Event, '/events/<event_id>')




if __name__ == "__main__":
    app.run(debug=True)
